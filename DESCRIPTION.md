This app packages Invoice Ninja <upstream>4.1.2</upstream>.

Invoice from anywhere, get paid from everywhere.

Invoice Ninja is the leading open-source platform providing powerful invoicing software for small businesses and freelancers.

Send invoices and manage payments from multiple devices in any location. Whether you’re in the office, visiting a client, or relaxing on vacation, you can monitor your records and track your invoices via your tablet or smartphone.