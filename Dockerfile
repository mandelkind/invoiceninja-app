FROM cloudron/base:0.11.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

#####
# SYSTEM REQUIREMENTS
#####

ENV PHANTOMJS phantomjs-2.1.1-linux-x86_64
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libmcrypt-dev zlib1g-dev git libgmp-dev \
        libfreetype6-dev libjpeg-turbo8-dev libpng12-dev \
        build-essential chrpath libssl-dev libxft-dev \
        libfreetype6 libfontconfig1 libfontconfig1-dev \
    && ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/local/include/ \
    && curl -o ${PHANTOMJS}.tar.bz2 -SL https://bitbucket.org/ariya/phantomjs/downloads/${PHANTOMJS}.tar.bz2 \
    && tar xvjf ${PHANTOMJS}.tar.bz2 \
    && rm ${PHANTOMJS}.tar.bz2 \
    && mv ${PHANTOMJS} /usr/local/share \
    && ln -sf /usr/local/share/${PHANTOMJS}/bin/phantomjs /usr/local/bin \
    && rm -rf /var/lib/apt/lists/*

#####
# COMPOSER
#####

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#####
# INVOICE NINJA
#####

ENV VERSION 4.1.4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN mkdir -p /run/invoiceninja/sessions
ADD env.template /app/code/.env.template

# make sure to change ownership on symlinks using `chown -h www-data:www-data ...`, otherwise php refuses to include files within them:
# https://serverfault.com/questions/393240/how-do-i-resolve-a-php-error-failed-opening-required-in-a-symlink-context

RUN curl -o invoiceninja.tar.gz -SL https://github.com/hillelcoren/invoice-ninja/archive/v${VERSION}.tar.gz \
    && tar -xzf invoiceninja.tar.gz -C /tmp/ \
    && rm invoiceninja.tar.gz \
    && mv /tmp/invoiceninja-${VERSION}/* /run/invoiceninja/ \
    && ln -s /run/invoiceninja/public /app/code/public \
    && chown -R www-data:www-data /run/invoiceninja \
    && sudo -u www-data composer install --working-dir=/run/invoiceninja --no-dev --no-interaction --no-progress \
    && mv /run/invoiceninja/storage /app/code/storage-vanilla && ln -s /app/data/storage /run/invoiceninja/storage && chown -h www-data:www-data /run/invoiceninja/storage \
    && mv /run/invoiceninja/public/logo /app/code/public-logo-vanilla && ln -s /app/data/public/logo /run/invoiceninja/public/logo && chown -h www-data:www-data /run/invoiceninja/public/logo \
    && rm -rf /run/invoiceninja/docs

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/invoiceninja.conf /etc/apache2/sites-enabled/invoiceninja.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php. apache2ctl -M can be used to list enabled modules
RUN a2enmod rewrite && \
    a2enmod expires && \
    a2enmod headers && \
    a2enmod cache
RUN crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_filesize 500M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP post_max_size 500M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP max_input_vars 1800 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/invoiceninja/sessions && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_divisor 100

# Hack to stop sendmail/postfix to keep retrying sending emails because of rofs
RUN rm -rf /var/spool

ADD start.sh /app/code/

RUN chown -R www-data:www-data /app/code /run/invoiceninja
RUN chmod +x /app/code/start.sh

CMD [ "/app/code/start.sh" ]