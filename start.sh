#!/bin/bash
set -eu

readonly ARTISAN="sudo -u www-data php /run/invoiceninja/artisan"
readonly COMPOSER="sudo -u www-data composer --working-dir=/run/invoiceninja"

# Settings which should be updated only once
if [[ ! -f "/app/data/.env" ]]; then
    sed -e "s|.*\(API_SECRET\).*|\1=$(pwgen -1cns 32)|g" \
        -e "s|.*\(MAIL_FROM_NAME\).*|\1=InvoiceNinja|g" \
        -e "s|.*\(PRECONFIGURED_INSTALL\).*|\1=true|g" \
        /app/code/.env.template > /app/data/.env # sed -i seems to destroy symlink
fi

# Settings to be updated on every run.
sed -e "s|.*\(APP_URL\).*|\1=${APP_ORIGIN}|g" \
    -e "s|.*\(DB_TYPE\).*|\1=mysql|g" \
    -e "s|.*\(DB_HOST\).*|\1=${MYSQL_HOST}:${MYSQL_PORT}|g" \
    -e "s|.*\(DB_DATABASE\).*|\1=${MYSQL_DATABASE}|g" \
    -e "s|.*\(DB_USERNAME\).*|\1=${MYSQL_USERNAME}|g" \
    -e "s|.*\(DB_PASSWORD\).*|\1=${MYSQL_PASSWORD}|g" \
    -e "s|.*\(MAIL_DRIVER\).*|\1=smtp|g" \
    -e "s|.*\(MAIL_PORT\).*|\1=${MAIL_SMTP_PORT}|g" \
    -e "s|.*\(MAIL_ENCRYPTION\).*|\1=|g" \
    -e "s|.*\(MAIL_HOST\).*|\1=${MAIL_SMTP_SERVER}|g" \
    -e "s|.*\(MAIL_USERNAME\).*|\1=${MAIL_SMTP_USERNAME}|g" \
    -e "s|.*\(MAIL_FROM_ADDRESS\).*|\1=${MAIL_FROM}|g" \
    -e "s|.*\(MAIL_PASSWORD\).*|\1=${MAIL_SMTP_PASSWORD}|g" \
    -e "s|.*\(REQUIRE_HTTPS\).*|\1=true|g" \
    -e "s|.*\(PHANTOMJS_CLOUD_KEY.*\)|#\1|g" \
    -e "s|.*\(PHANTOMJS_BIN_PATH\).*|\1=\/usr/local/bin/phantomjs|g" \
    /app/data/.env > /run/invoiceninja/.env # sed -i seems to destroy symlink

# ensure permissions are set correctly
chown -R www-data:www-data /app/data /run/invoiceninja

# ensure permissions are set correctly
chown -R www-data:www-data /app/data /run/invoiceninja

if [[ ! -f "/app/data/.dbsetup" ]]; then
    echo "Copying files on first run"
    cp -r /app/code/storage-vanilla /app/data/storage    
    chown -R www-data:www-data /app/data/storage

    mkdir -p /app/data/public
    cp -r /app/code/public-logo-vanilla /app/data/public/logo
    chown -R www-data:www-data /app/data/public

    $COMPOSER dump-autoload --optimize --no-interaction
    $ARTISAN optimize --force --no-interaction --verbose
    $ARTISAN migrate --force --no-interaction --verbose
    $ARTISAN db:seed --force --no-interaction --verbose

    touch "/app/data/.dbsetup"
else
    # Put the application into maintenance mode
    $ARTISAN down --no-interaction --verbose

    # Run the database migrations
    $ARTISAN migrate --force --no-interaction --verbose

    # Optimize the framework for better performance
    $ARTISAN optimize --force --no-interaction --verbose

    # Bring the application out of maintenance mode
    $ARTISAN up --no-interaction --verbose
fi

# Update application key
$ARTISAN ninja:update-key --no-interaction --verbose

# start
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND