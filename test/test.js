#!/usr/bin/env node
'use strict'

/* global describe, after, xit, it */

require('chromedriver')

const execSync = require('child_process').execSync
const expect = require('expect.js')
// const net = require('net')
const path = require('path')

const selenium = require('selenium-webdriver')

const {By, until} = selenium

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
selenium.promise.USE_PROMISE_MANAGER = false

describe('Application life cycle test', function () {
  this.timeout(0)

  const chrome = require('selenium-webdriver/chrome')
  const browser = new selenium.Builder()
    .forBrowser('chrome')
    .setChromeOptions(new chrome.Options().addArguments(['no-sandbox', 'headless']))
    .build()
  const firstname = 'testfirstname'
  const lastname = 'testlastname'
  const email = 'user@example.com'
  const password = 'password'

  after(function () {
    browser.quit()
  })

  const LOCATION = 'test'
  const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000
  let app

  function waitForElement (elem) {
    return browser.wait(until.elementLocated(elem), TEST_TIMEOUT)
      .then(() => browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT))
  }

  function assertElementText (elem, supposedText) {
    return browser.findElement(elem).getText()
      .then(text => {
        if (text === supposedText) return true
        else throw new Error(`Assertion error. Expected text '${supposedText}'. Got '${text}.'`)
      })
  }

  function getAppInfo () {
    const inspect = JSON.parse(execSync('cloudron inspect'))
    app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0]
    expect(app).to.be.an('object')
  }

  function register () {
    return browser.manage().deleteAllCookies()
      .then(() => browser.get('https://' + app.fqdn))
      .then(() => browser.wait(until.elementLocated(By.id('first_name')), 60000)) // first load can be *slow*
      .then(() => browser.findElement(By.id('first_name')).sendKeys(firstname))
      .then(() => browser.findElement(By.id('last_name')).sendKeys(lastname))
      .then(() => browser.findElement(By.id('email')).sendKeys(email))
      .then(() => browser.findElement(By.id('password')).sendKeys(password))
      .then(() => browser.findElement(By.id('terms_checkbox')).click())
      .then(() => browser.findElement(By.css('.btn-lg')).click())
      .then(() => waitForElement(By.id('loginButton')))
  }

  function login () {
    return browser.manage().deleteAllCookies()
      .then(() => browser.get('https://' + app.fqdn))
      .then(() => waitForElement(By.id('loginButton')))
      .then(() => browser.findElement(By.id('email')).sendKeys(email))
      .then(() => browser.findElement(By.id('password')).sendKeys(password))
      .then(() => browser.findElement(By.id('loginButton')).click())
      .then(() => waitForElement(By.id('myAccountButton')))
  }

  function logout () {
    browser.get('https://' + app.fqdn)

    return waitForElement(By.id('myAccountButton'))
      .then(() => browser.findElement(By.id('myAccountButton')).click())
      .then(() => waitForElement(By.css('ul.dropdown-menu > li:nth-child(4) > a:nth-child(1)')))
      .then(() => browser.findElement(By.css('ul.dropdown-menu > li:nth-child(4) > a:nth-child(1)')).click())
  }

  function createInvoice () {
    browser.get('https://' + app.fqdn)

    return waitForElement(By.css('.nav-invoices > a:nth-child(2)')) // "Invoice" link in sidebar
      .then(() => console.log('Page loaded'))
      .then(() => browser.findElement(By.css('.nav-invoices > a:nth-child(2)')).click())
      .then(() => waitForElement(By.css('#top_right_buttons > a:nth-child(2)'))) // "New Invoice" button
      .then(() => console.log('Invoice Page loaded'))
      .then(() => browser.findElement(By.css('#top_right_buttons > a:nth-child(2)')).click())
      .then(() => waitForElement(By.css('form.form-horizontal:nth-child(5)'))) // new invoice form
      .then(() => console.log('New Invoice Page loaded'))
      .then(() => browser.findElement(By.id('createClientLink')).click()) // open new client modal
      .then(() => console.log('Opening new client modal'))
      .then(() => waitForElement(By.id('client[name]')))
      .then(() => console.log('Opened new client modal'))
      .then(() => browser.findElement(By.id('client[name]')).sendKeys('testclient'))
      .then(() => browser.findElement(By.id('clientDoneButton')).click()) // adding client
      .then(() => console.log('New client added'))
      .then(() => browser.findElement(By.css('input.invoice-item:nth-child(2)')).sendKeys('testitem')) // item name : testitem
      .then(() => console.log('item name entered'))
      .then(() => browser.findElement(By.css('tr.sortable-row:nth-child(1) > td:nth-child(4) > input:nth-child(1)')).sendKeys('42')) // $42
      .then(() => console.log('item price entered'))
      .then(() => browser.findElement(By.css('tr.sortable-row:nth-child(1) > td:nth-child(5) > input:nth-child(1)')).sendKeys('1')) // 1 unit
      .then(() => console.log('item number entered'))
      .then(() => browser.findElement(By.id('draftButton')).click()) // save the draft
  }

  function checkInvoiceExists () {
    browser.get('https://' + app.fqdn)

    return waitForElement(By.css('.nav-invoices > a:nth-child(2)')) // "Invoice" link in sidebar
      .then(() => console.log('Page loaded'))
      .then(() => browser.findElement(By.css('.nav-invoices > a:nth-child(2)')).click())
      .then(() => waitForElement(By.css('#top_right_buttons > a:nth-child(2)'))) // "New Invoice" button
      .then(() => console.log('Invoice Page loaded'))
      .then(() => waitForElement(By.css('.odd > td:nth-child(3) > a:nth-child(1)')))
      .then(() => console.log('found invoice line'))
      .then(() => assertElementText(By.css('.odd > td:nth-child(3) > a:nth-child(1)'), 'testclient'))
      .then(() => console.log('client name is as expected'))
      .then(() => assertElementText(By.css('.odd > td:nth-child(5)'), '$42.00'))
      .then(() => console.log('price is as expected'))
  }

  it('build app', function () {
    execSync('cloudron build', {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('install app', function () {
    execSync('cloudron install --new --wait --location ' + LOCATION, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })

  it('can get app information', getAppInfo)

  it('can register', register)
  it('can login', login)
  it('can create invoice', createInvoice)
  it('invoice exists', checkInvoiceExists)
  it('can logout', logout)

  it('can restart app', function () {
    execSync('cloudron restart --wait --app ' + app.id)
  })

  it('can login', login)
  it('invoice exists', checkInvoiceExists)
  it('can logout', logout)

  it('backup app', function () {
    execSync('cloudron backup create --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('restore app', function () {
    execSync('cloudron restore --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  it('can login', login)
  it('invoice exists', checkInvoiceExists)
  it('can logout', logout)

  it('move to different location', function () {
    execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })
  it('can get new app information', getAppInfo)

  it('can login', login)
  it('invoice exists', checkInvoiceExists)
  it('can logout', logout)

  it('uninstall app', function () {
    execSync('cloudron uninstall --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  // test update (this test will only work after app is published)
  xit('can install app', function () {
    execSync('cloudron install --new --wait --appstore-id com.invoiceninja.cloudronapp --location ' + LOCATION, {
      cwd: path.resolve(__dirname, '..'),
      stdio: 'inherit'
    })
  })

  xit('can get app information', getAppInfo)
  xit('can register', register)
  xit('can login', login)
  xit('can create invoice', createInvoice)
  xit('invoice exists', checkInvoiceExists)
  xit('can logout', logout)

  xit('can update', function () {
    execSync('cloudron install --wait --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })

  xit('can login', login)
  xit('invoice exists', checkInvoiceExists)
  xit('can logout', logout)

  xit('uninstall app', function () {
    execSync('cloudron uninstall --app ' + app.id, {cwd: path.resolve(__dirname, '..'), stdio: 'inherit'})
  })
})
